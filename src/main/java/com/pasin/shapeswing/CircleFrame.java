/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Pla
 */
public class CircleFrame extends JFrame {

    JLabel lbRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lbResult;

    public CircleFrame() {
        super("Circle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lbRadius = new JLabel("Radius", JLabel.TRAILING);
        lbRadius.setSize(50, 20);
        lbRadius.setLocation(5, 5);
        lbRadius.setBackground(Color.WHITE);
        lbRadius.setOpaque(true);
        this.add(lbRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lbResult = new JLabel("Circle Radius= ??? Area= ??? Perimeter= ???");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(400, 50);
        lbResult.setLocation(0, 50);
        lbResult.setBackground(Color.yellow);
        lbResult.setOpaque(true);
        this.add(lbResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtRadius.getText();
                    double radius = Double.parseDouble(strRadius);
                    Circle circle = new Circle(radius);
                    lbResult.setText("Cirle Radius = " + String.format("%.2f", circle.getRadius())
                            + " Area = " + String.format("%.2f", circle.calArea())
                            + " Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(CircleFrame.this, "Error.Please Input Number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();

                }
            }

        });
    }

    public static void main(String[] args) {
        CircleFrame frame = new CircleFrame();
        frame.setVisible(true);
    }
}
