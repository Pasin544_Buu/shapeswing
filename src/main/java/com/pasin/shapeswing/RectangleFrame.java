/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Pla
 */
public class RectangleFrame extends JFrame {
    
    JLabel lbBase;
    JTextField txtBase;
    JLabel lbHigh;
    JTextField txtHigh;
    JButton btnCalculate;
    JLabel lbResult;
    
    public RectangleFrame() {
        super("Rectangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lbBase = new JLabel("Base", JLabel.TRAILING);
        lbBase.setSize(30, 20);
        lbBase.setLocation(5, 5);
        lbBase.setBackground(Color.WHITE);
        lbBase.setOpaque(true);
        this.add(lbBase);
        
        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(40, 5);
        this.add(txtBase);
        
        lbHigh = new JLabel("High", JLabel.TRAILING);
        lbHigh.setSize(25, 20);
        lbHigh.setLocation(5, 25);
        lbHigh.setBackground(Color.WHITE);
        lbHigh.setOpaque(true);
        this.add(lbHigh);
        
        txtHigh = new JTextField();
        txtHigh.setSize(50, 20);
        txtHigh.setLocation(40, 25);
        this.add(txtHigh);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(100, 12);
        this.add(btnCalculate);
        
        lbResult = new JLabel("Rectangle Base= ??? High= ??? Area= ??? Perimeter= ???");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(400, 50);
        lbResult.setLocation(0, 50);
        lbResult.setBackground(Color.pink);
        lbResult.setOpaque(true);
        this.add(lbResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHigh = txtHigh.getText();
                    double base = Double.parseDouble(strBase);
                    double high = Double.parseDouble(strHigh);
                    Rectangle rectangle = new Rectangle(base, high);
                    lbResult.setText("Rectangle Base = " + String.format("%.2f", rectangle.getBase())
                            + " High = " + String.format("%.2f", rectangle.getHigh())
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter " + String.format("%.2f", rectangle.calPerimeter()));
                    
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error.Please Input Number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHigh.setText("");
                    txtBase.requestFocus();
                }
            }
            
        });
    }
    
    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
