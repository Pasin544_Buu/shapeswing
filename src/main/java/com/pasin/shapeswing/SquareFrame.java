/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Pla
 */
public class SquareFrame extends JFrame {

    JLabel lbSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lbResult;

    public SquareFrame() {
        super("Square");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lbSide = new JLabel("Side", JLabel.TRAILING);
        lbSide.setSize(30, 20);
        lbSide.setLocation(5, 5);
        lbSide.setBackground(Color.WHITE);
        lbSide.setOpaque(true);
        this.add(lbSide);

        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(40, 5);
        this.add(txtSide);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(100, 5);
        this.add(btnCalculate);

        lbResult = new JLabel("Square Side= ??? Area= ??? Perimeter= ???");
        lbResult.setHorizontalAlignment(JLabel.CENTER);
        lbResult.setSize(400, 50);
        lbResult.setLocation(0, 50);
        lbResult.setBackground(Color.green);
        lbResult.setOpaque(true);
        this.add(lbResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lbResult.setText("Square Side = " + String.format("%.2f", square.getSide())
                            + " Area = " + String.format("%.2f", square.calArea())
                            + " Perimeter = " + String.format("%.2f", square.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error.Please Input Number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }

}
