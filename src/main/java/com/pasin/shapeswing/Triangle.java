/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeswing;

/**
 *
 * @author Pla
 */
public class Triangle extends Shape {

    private double base;
    private double high;

    public Triangle(double base, double high) {
        super("Triangle");
        this.base = base;
        this.high = high;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    @Override
    public double calArea() {
        return 0.5 * base * high;
    }

    @Override
    public double calPerimeter() {
        return base * 3;
    }

}
