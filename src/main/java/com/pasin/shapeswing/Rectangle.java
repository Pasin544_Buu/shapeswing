/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeswing;

/**
 *
 * @author Pla
 */
public class Rectangle extends Shape {

    private double base;
    private double high;

    public Rectangle(double base, double high) {
        super("Rectangle");
        this.base = base;
        this.high = high;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    @Override
    public double calArea() {
        return base * high;
    }

    @Override
    public double calPerimeter() {
        return base + base + high + high;
    }

}
